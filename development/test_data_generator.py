from datetime import date

from faker import Faker

import jsonpickle

fake = Faker('pl_PL')

def generate_users(n):
    with open("test_data/users.json", "w") as f:
        user = {}
        for _ in range(n):
            user['first_name'] = fake.first_name()
            user['last_name'] = fake.last_name()
            user['born_date'] = fake.date()
            user['email'] = fake.email()
            f.write(jsonpickle.encode(user) + '\n')


    # x = Test(1)
    # y = jsonpickle.encode(x)
    # print(y)
    # z = jsonpickle.decode(y)
    #
    # assert x.x == z.x

def generate_teams(n):
    return ("Team(name='{}', city='{}', description='{}'").format(fake.last_name(), fake.city(), fake.words())


if __name__ == "__main__":
    generate_users(100)

    # print(dir(fake))