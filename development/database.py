from datetime import date
from random import randint

import flask_sqlalchemy
import jsonpickle
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

from config import BaseConfig
from helpers import password_helpers, location_helpers
from model import enums
from model.enums import Disciplines

engine = create_engine(BaseConfig.SQLALCHEMY_DATABASE_URI, convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine, query_cls=flask_sqlalchemy.BaseQuery))
Base = declarative_base()
Base.query = db_session.query_property()


def fill_db():
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()

    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)

    db_session.add_all(get_user_data())
    db_session.add_all(get_teams_data())
    db_session.flush()
    db_session.add_all(get_team_membership_data())
    db_session.flush()

    db_session.add_all(get_test_users())
    db_session.flush()
    db_session.add_all(get_test_users_team_memberships())
    db_session.flush()

    test_notifications = get_test_notifications()
    db_session.add_all(test_notifications[0])
    db_session.add_all(test_notifications[1])
    db_session.commit()


def get_test_notifications():
    from model.db.models import TeamMembership, Team
    from services import notification_service
    team = Team.query.filter_by(id=1).first()
    tm1 = TeamMembership.query.filter_by(user_id=102).first()
    tm2 = TeamMembership.query.filter_by(user_id=103).first()
    notification1 = notification_service.get_team_join_request_notifications(tm1.user, team, tm1)
    notification2 = notification_service.get_team_join_request_notifications(tm2.user, team, tm2)
    return notification1, notification2


def get_test_users():
    from model.db.models import User
    salt = password_helpers.get_random_salt()
    return [
        User(email='test1@gmail.com', first_name="First", last_name="User",password=password_helpers.get_hashed_password('123', salt), salt=salt, confirmed=True),
        User(email='test2@gmail.com', first_name="Second", last_name="User",password=password_helpers.get_hashed_password('123', salt), salt=salt, confirmed=True),
        User(email='test3@gmail.com', first_name="Third", last_name="User",password=password_helpers.get_hashed_password('123', salt), salt=salt, confirmed=True)
    ]


def get_test_users_team_memberships():
    from model.db.models import User, TeamMembership, Team
    team1 = Team.query.filter_by(id=1).first()
    team2 = Team.query.filter_by(id=2).first()
    user1 = User.query.filter_by(email='test1@gmail.com').first()
    user2 = User.query.filter_by(email='test2@gmail.com').first()
    user3 = User.query.filter_by(email='test3@gmail.com').first()
    from datetime import datetime
    return [
        TeamMembership(team=team1, user=user1, confirmed=True, role=enums.Roles.LEADER, start_date=datetime.utcnow()),
        TeamMembership(team=team2, user=user2, confirmed=True, role=enums.Roles.LEADER, start_date=datetime.utcnow()),
        TeamMembership(team=team1, user=user2),
        TeamMembership(team=team1, user=user3)
    ]


def get_user_data():
    from model.db.models import User
    users = []
    with open("development/test_data/users.json", 'r') as f:
        for line in f:
            dto = jsonpickle.decode(line)
            user = User()
            user.first_name = dto['first_name']
            user.last_name = dto['last_name']
            user.born_date = dto['born_date']
            user.email = dto['email']
            users.append(user)

    return users


def get_teams_data():
    from model.db.models import Team
    teams = []
    with open("development/test_data/teams.json", 'r') as f:
        for line in f:
            dto = jsonpickle.decode(line)
            team = Team()
            team.name = dto['name']
            team.location = dto['city']
            team.lat, team.lng = location_helpers.get_coordinates_from_location(team.location)
            team.discipline = Disciplines(dto['discipline'])
            teams.append(team)
    return teams


def get_team_membership_data():
    from model.db.models import TeamMembership, Team, User
    team_no = db_session.query(Team).count()
    users_no = db_session.query(User).count()
    team_membership_list = []
    for user_id in range(1, users_no):
        team_membership_list.append(
            TeamMembership(confirmed=True, user=db_session.query(User).filter(User.id == user_id).first(),
                           team=db_session.query(Team).filter(Team.id == randint(1, team_no)).first(),
                           start_date=date.today()))

    return team_membership_list
