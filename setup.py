from setuptools import setup

setup(
    name='DemCouch',
    version='1.0',
    long_description=__doc__,
    packages=['development', 'controllers', 'model'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[]
)