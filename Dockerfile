FROM python:3.6-alpine
RUN adduser -D sstrzelka

WORKDIR /home/microblog

COPY requirements.txt requirements.txt
RUN apk update && \
 apk add postgresql-libs && \
 apk add --virtual .build-deps gcc musl-dev postgresql-dev && \
 python3 -m pip install -r requirements.txt --no-cache-dir && \
 apk --purge del .build-deps

#install dependencies specified in requirements.txt
RUN pip install gunicorn

COPY . .
RUN chmod +x boot.sh

ENV FLASK_APP webapp.py
RUN chown -R sstrzelka:sstrzelka ./
USER sstrzelka

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]
