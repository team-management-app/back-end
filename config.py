import os


class BaseConfig(object):

    APP_VERSION = os.environ.get('APP_VERSION') or 0.1
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'postgresql://sstrzelka:12345@localhost/demcouch_db'

    # pagination
    ITEMS_PER_PAGE = 10

    # thumbnails
    UPLOAD_FOLDER = 'static/thumbnails'
    ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}
    MAX_CONTENT_LENGTH = 2 * 1024 * 1024

    # security
    SECURITY_PASSWORD_SALT = os.environ.get('PASSWORD_SALT')
    SECRET_KEY = os.environ.get('SECRET_KEY')

    # mail settings
    MAIL_SERVER = 'smtp.googlemail.com'
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True

    # gmail authentication
    MAIL_USERNAME = 'demcouch'
    MAIL_PASSWORD = os.environ.get('APP_MAIL_PASSWORD')

    # mail accounts
    MAIL_DEFAULT_SENDER = 'demcouch@gmail.com'
