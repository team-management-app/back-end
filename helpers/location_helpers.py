from geopy import Nominatim, Point, distance

from exceptions.exceptions import CannotDetermineExactLocationException

geolocator = Nominatim(user_agent='demcouch')


def get_location_from_coordinates(lat, long):
    try:
        location = geolocator.reverse(query=Point(lat, long), language='pl')
    except ValueError as exc:
        raise CannotDetermineExactLocationException(msg=str(exc))
    if 'city' in location.raw.get('address', {}):
        return location.raw['address']['city']
    elif 'town' in location.raw.get('address', {}):
        return location.raw['address']['town']
    elif 'village' in location.raw.get('address', {}):
        return location.raw['address']['village']
    raise CannotDetermineExactLocationException


def get_distance_between_coordinates(first, second):
    return distance.great_circle(first, second).kilometers


def get_coordinates_from_location(location):
    coordinates = geolocator.geocode(location)
    return coordinates.latitude, coordinates.longitude


def set_team_location(coordinates, team):
    if coordinates is not None:
        if 'name' in coordinates:
            team.location = coordinates['name']
            team.lat, team.lng = get_coordinates_from_location(team.location)
        elif 'lat' and 'lng' in coordinates:
            team.lat = coordinates['lat']
            team.lng = coordinates['lng']
            team.location = get_location_from_coordinates(coordinates['lat'], coordinates['lng'])

