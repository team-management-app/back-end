import os
import random
import string

from flask import current_app, request
from werkzeug.utils import secure_filename

from development.database import db_session
from exceptions.exceptions import InvalidDataException, MissingRequiredArgumentException, ResourceNotFoundException


def check_if_file_is_allowed(filename):
    if '.' in filename and filename.rsplit('.', 1)[1].lower() in current_app.config['ALLOWED_EXTENSIONS']:
        pass
    else:
        raise InvalidDataException("Invalid file extension. Must be one of {}".format(current_app.config['ALLOWED_EXTENSIONS']))


def is_filename_unique(filename):
    # TODO
    # check sha of uploaded file
    existing_filenames = os.listdir(current_app.config['UPLOAD_FOLDER'])
    return filename not in existing_filenames


def append_random_string(filename):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10)) + filename


def parse_and_save_thumbnail(record_id, cls):
    if 'file' not in request.files:
        raise MissingRequiredArgumentException(msg="Missing required argument 'file'")
    file = request.files['file']
    # if user does not select file, browser also
    # submit a empty part without filename
    if file.filename == '':
        raise MissingRequiredArgumentException(msg="Missing required argument 'file' or filename is empty")
    check_if_file_is_allowed(file.filename)
    record = cls.query.filter(cls.id == record_id).first()
    if not record:
        raise ResourceNotFoundException(cls=cls, value=record_id)

    filename = secure_filename(file.filename)
    if not is_filename_unique(filename):
        filename = append_random_string(filename)
    file_path = os.path.join(current_app.config['UPLOAD_FOLDER'], filename)
    file.save(file_path)
    record.picture = file_path
    db_session.commit()
    return file_path


def parse_and_delete_thumbnail(record_id, cls):
    record = cls.query.filter(cls.id == record_id).first()
    if not record:
        raise ResourceNotFoundException(cls=cls, value=record_id)
    if not record.picture:
        raise InvalidDataException("Record does not have any thumbnail")
    os.remove(record.picture)
    record.picture = None
    db_session.commit()
