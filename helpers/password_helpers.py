import hashlib
import random
import string


def get_hashed_password(clear_password, salt):
    text_to_hash = clear_password + salt
    return hashlib.sha256(text_to_hash.encode('utf-8')).hexdigest()


def get_random_salt():
    return ''.join(random.choice(string.ascii_uppercase + string.digits + string.ascii_lowercase) for _ in range(10))