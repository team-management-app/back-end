from exceptions.exceptions import InsufficientPermissionsException
from model import enums
from model.db.models import TeamMembership
from services import token_service


def is_current_user_team_leader(team):
    membership = TeamMembership.query.filter_by(team_id=team.id, user_id=token_service.get_current_user_id()).first()
    if not membership:
        return False
    return membership.role == enums.Roles.LEADER


def verify_user_belongs_to_one_of_the_teams(teams):
    user_id = token_service.get_current_user_id()
    count = 0
    for team in teams:
        count += TeamMembership.query.filter_by(team_id=team.id, user_id=user_id).count()
    if not count:
            raise InsufficientPermissionsException()
