from exceptions.exceptions import MissingRequiredArgumentException
from helpers import validation_helpers


def get_email_password_from_request(request):
    try:
        return request['email'], request['password']
    except KeyError as exc:
        raise MissingRequiredArgumentException(msg='Missing required argument {}'.format(str(exc)))


def filter_matches_by_time(matches, time):
    filtered_matches = []
    if time == 'past':
        for match in matches:
            if validation_helpers.is_datetime_past(match.date):
                filtered_matches.append(match)
    elif time == 'future':
        for match in matches:
            if not validation_helpers.is_datetime_past(match.date):
                filtered_matches.append(match)
    else:
        return matches
    return filtered_matches
