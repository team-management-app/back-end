import datetime

import dateutil.parser

from exceptions.exceptions import InvalidDataException


def validate_date(date_text):
    try:
        datetime.datetime.strptime(date_text, '%Y-%m-%d')
    except (ValueError, TypeError):
        raise InvalidDataException("Invalid date format, should be YYYY-MM-DD", status_code=422)


def validate_datetime_is_iso_format(date):
    try:
        dateutil.parser.parse(date)
        time = date.split('T')[1].split('+')[0]
    except (ValueError, IndexError):
        raise InvalidDataException(msg='Invalid ISO datetime format', status_code=422)

    try:
        datetime.datetime.strptime(time, '%H:%M:%S')
        return
    except ValueError:
        try:
            datetime.datetime.strptime(time, '%H:%M')
            return
        except ValueError:
            raise InvalidDataException(msg='Invalid ISO datetime format', status_code=422)


def validate_datetime_is_not_past(date_str):
    date = dateutil.parser.parse(date_str)
    # datetime with offset
    if date.tzinfo is not None and date.tzinfo.utcoffset(date) is not None:
        if not datetime.datetime.now(datetime.timezone.utc) < date:
            raise InvalidDataException(msg='Date cannot be past', status_code=422)

    # native datetime
    if date.tzinfo is None or date.tzinfo.utcoffset(date) is None:
        if not datetime.datetime.now() < date:
            raise InvalidDataException(msg='Date cannot be past', status_code=422)


def is_datetime_past(date):
    if date.tzinfo is not None and date.tzinfo.utcoffset(date) is not None:
        return datetime.datetime.now(datetime.timezone.utc) > date

    if date.tzinfo is None or date.tzinfo.utcoffset(date) is None:
        return datetime.datetime.now() > date
