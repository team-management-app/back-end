from sqlalchemy.exc import IntegrityError

from development.database import db_session
from exceptions.exceptions import InvalidDataException
from model.db import models

CLASSES = {
    "Team": models.Team,
    "TeamMembership": models.TeamMembership,
    "User": models.User,
    "Notification": models.Notification,
    "MatchInvitation": models.MatchInvitation,
    "Match": models.Match
}

def commit_unique_safe():
    try:
        db_session.commit()
    except IntegrityError as exc:
        start_index = str(exc).find('DETAIL')
        end_index = str(exc).find('\n', start_index)
        msg = 'Duplicated unique value! ' + str(exc)[start_index:end_index]
        raise InvalidDataException(msg=msg, status_code=409)


def get_class_from_name(name):
    return CLASSES[name]
