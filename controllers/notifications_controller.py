from flask import Blueprint, jsonify, Response

from controllers.user_controller import is_current_user
from development import database
from exceptions.exceptions import InsufficientPermissionsException, ResourceNotFoundException
from helpers import db_helpers
from model.db import models
from model.decorators import authentication_required, account_confirmation_required

notifications_view = Blueprint('notifications', __name__)


@notifications_view.route('/users/<user_id>/notifications')
@authentication_required
@account_confirmation_required
def get_user_notifications(user_id):
    if not is_current_user(user_id):
        raise InsufficientPermissionsException()

    user = models.User.query.filter_by(id=user_id).first()
    if not user:
        raise ResourceNotFoundException(cls=models.User, value=user_id)
    notifications = user.notifications
    delete_list = []

    for notification in notifications:
        for key, value in notification.data.items():
            cls = db_helpers.get_class_from_name(value['cls'])
            entity = cls.query.filter_by(id=value['id']).first()
            if not entity:
                delete_list.append(notification)
                continue
            notification.data[key] = entity.serializable

    notifications = [x for x in notifications if x not in delete_list]
    notifications.sort(key=lambda n: n.create_date, reverse=True)
    return jsonify([dict(notification.serializable) for notification in notifications])


@notifications_view.route('/notifications/<notification_id>', methods=['DELETE'])
@authentication_required
@account_confirmation_required
def delete_notification(notification_id):
    notification = models.Notification.query.filter_by(id=notification_id).first()
    if not notification:
        raise ResourceNotFoundException(cls=models.Notification, value=notification_id)
    if not is_current_user(notification.recipient_id):
        raise InsufficientPermissionsException()

    database.db_session.delete(notification)
    database.db_session.commit()
    return Response(status=204)
