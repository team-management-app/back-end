from flask import Blueprint, jsonify

from exceptions.exceptions import ResourceNotFoundException
from helpers import permission_helpers
from model.db.models import Match
from model.decorators import authentication_required, account_confirmation_required

match_view = Blueprint(name='match', import_name=__name__)


@match_view.route('/matches/<match_id>')
@authentication_required
@account_confirmation_required
def get_single_match(match_id):
    match = Match.query.filter_by(id=match_id).first()
    if not match:
        raise ResourceNotFoundException(cls=Match, value=match_id)

    permission_helpers.verify_user_belongs_to_one_of_the_teams([match.host, match.away])

    return jsonify(match.serializable)