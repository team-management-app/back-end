from flask import Blueprint, jsonify, current_app

from development.database import db_session

health_view = Blueprint(name='heath', import_name=__name__)


def check_database_connection():
    try:
        # to check database we will execute raw query
        db_session.execute('SELECT 1')
        return 'OK'
    except:
        return 'ERROR'


@health_view.route('/health')
def hello_world():
    return jsonify(dict(version=current_app.config['APP_VERSION'], database_connection=check_database_connection()))
