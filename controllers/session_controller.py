from flask import Blueprint, request, jsonify, Response

from development.database import db_session
from exceptions.exceptions import InvalidDataException, ResourceNotFoundException, AccountNotConfirmedException
from helpers import request_helpers, password_helpers
from model.db.models import User, BlacklistToken
from model.decorators import authentication_required
from services import token_service

session_view = Blueprint('session', __name__)


@session_view.route('/session', methods=['POST'])
def get_new_session():
    email, password = request_helpers.get_email_password_from_request(request.get_json())
    user = User.query.filter(User.email == email).first()
    if not user:
        raise ResourceNotFoundException(cls=User, key='email', value=email)
    if not user.confirmed:
        raise AccountNotConfirmedException()
    if not password_helpers.get_hashed_password(password, user.salt) == user.password:
        raise InvalidDataException(msg='Provided password is incorrect', status_code=422)

    auth_token = token_service.encode_auth_token(user.id).decode()
    return jsonify(dict(auth_token=auth_token, user_id=user.id))


@session_view.route('/session', methods=['DELETE'])
@authentication_required
def delete_session():
    auth_token = token_service.get_token_from_header()
    blacklist_token = BlacklistToken(token=auth_token)
    db_session.add(blacklist_token)
    db_session.commit()
    return Response(status=204)

