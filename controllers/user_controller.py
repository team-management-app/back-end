from flask import Blueprint, request, jsonify, Response, url_for, render_template
from validate_email import validate_email

from development.database import db_session
from exceptions.exceptions import MissingRequiredArgumentException, InvalidDataException, \
    ResourceAlreadyExistsException, InsufficientPermissionsException, ResourceNotFoundException
from helpers import files_helpers, validation_helpers, db_helpers, password_helpers, request_helpers
from model.db.models import User, TeamMembership
from model.decorators import authentication_required, account_confirmation_required
from services import token_service, mail_service

user_view = Blueprint('user', __name__)


@user_view.route('/users', methods=['POST'])
def register_new_user():
    req_data = request.get_json()
    try:
        email = req_data['email']
        password = req_data['password']
        first_name = req_data['first_name']
        last_name = req_data['last_name']
    except KeyError as exc:
        raise MissingRequiredArgumentException(msg='Missing required argument {}'.format(str(exc)))

    # born date is optional
    if 'born_date' in req_data and req_data['born_date']:
        born_date = req_data['born_date']
        validation_helpers.validate_date(born_date)
    else:
        born_date = None

    if not validate_email(email):
        raise InvalidDataException(msg="Email is invalid", status_code=422)

    # TODO add password validation

    if User.query.filter(User.email == email).first():
        raise ResourceAlreadyExistsException(msg="User with this email already exists")

    salt = password_helpers.get_random_salt()
    hashed_password = password_helpers.get_hashed_password(password, salt)
    user = User(email=email, password=hashed_password, first_name=first_name, last_name=last_name,
                born_date=born_date, salt=salt)

    db_session.add(user)
    db_session.commit()
    send_confirmation_email(email)
    return jsonify(user.serializable_with_team_list)


@user_view.route('/users/<user_id>', methods=['GET'])
@authentication_required
@account_confirmation_required
def get_single_user(user_id):
    user = User.query.filter_by(id=user_id).first()
    if not user:
        raise ResourceNotFoundException(cls=User, value=user_id)
    response = user.serializable_with_team_list
    # add email only if request comes from account owner
    if is_current_user(user_id):
        response['email'] = user.email

    return jsonify(response)


@user_view.route('/users/<user_id>', methods=['PUT'])
@authentication_required
@account_confirmation_required
def update_user(user_id):
    user = User.query.filter_by(id=user_id).first()
    if not user:
        raise ResourceNotFoundException(cls=User, value=user_id)
    if not is_current_user(user_id):
        raise InsufficientPermissionsException()

    req_data = request.get_json()
    try:
        first_name = req_data['first_name']
        last_name = req_data['last_name']
        born_date = req_data['born_date']
    except KeyError as exc:
        raise MissingRequiredArgumentException(msg='Missing required argument {}'.format(str(exc)))

    if born_date:
        validation_helpers.validate_date(born_date)

    user.first_name = first_name
    user.last_name = last_name
    user.born_date = born_date

    db_helpers.commit_unique_safe()

    response = user.serializable
    response['email'] = user.email
    return jsonify(response)


@user_view.route('/users/<user_id>/password', methods=['PUT'])
@authentication_required
@account_confirmation_required
def change_user_password(user_id):
    user = User.query.filter_by(id=user_id).first()
    if not user:
        raise ResourceNotFoundException(cls=User, value=user_id)
    if not is_current_user(user_id):
        raise InsufficientPermissionsException()

    req_data = request.get_json()
    try:
        password = req_data['password']
    except KeyError as exc:
        raise MissingRequiredArgumentException(msg='Missing required argument {}'.format(str(exc)))

    user.password = password_helpers.get_hashed_password(password, user.salt)

    db_helpers.commit_unique_safe()
    response = user.serializable
    response['email'] = user.email
    return jsonify(response)


@user_view.route('/users/confirm/<token>')
def confirm_account(token):
    email = token_service.confirm_token(token)
    user = User.query.filter_by(email=email).first()
    if not user:
        raise ResourceNotFoundException(cls=User,key='email', value=email)
    if user.confirmed:
        raise ResourceAlreadyExistsException(msg='Account has already been confirmed')
    else:
        user.confirmed = True
        db_session.commit()
        return Response(status=204)


@user_view.route('/users/<user_id>/thumbnail', methods=['POST'])
@authentication_required
@account_confirmation_required
def upload_thumbnail(user_id):
    if not is_current_user(user_id):
        raise InsufficientPermissionsException()
    file_path = files_helpers.parse_and_save_thumbnail(user_id, User)
    return jsonify(thumbnail=file_path)


@user_view.route('/users/<user_id>/thumbnail', methods=['DELETE'])
@authentication_required
@account_confirmation_required
def delete_thumbnail(user_id):
    if not is_current_user(user_id):
        raise InsufficientPermissionsException()
    files_helpers.parse_and_delete_thumbnail(user_id, User)
    return Response(status=204)


@user_view.route('/users/<user_id>/team-memberships')
@authentication_required
@account_confirmation_required
def get_user_team_memberships(user_id):
    # filtering param
    team_id = request.args.get('team_id', default=None, type=int)

    memberships_query = TeamMembership.query.filter_by(user_id=user_id)
    if team_id:
        memberships_query = memberships_query.filter_by(team_id=team_id)
    memberships = memberships_query.all()
    return jsonify([dict(membership.serializable) for membership in memberships])


@user_view.route('/users/<user_id>/matches')
@authentication_required
@account_confirmation_required
def get_all_user_matches(user_id):
    user = User.query.filter_by(id=user_id).first()
    time = request.args.get('time', default='', type=str)

    if not user:
        raise ResourceNotFoundException(cls=User, value=user_id)

    memberships = user.contracts
    all_matches = set()
    for membership in memberships:
        all_matches.update(membership.team.matches)

    filtered_matches = request_helpers.filter_matches_by_time(all_matches, time)

    return jsonify([dict(m.serializable) for m in filtered_matches])


def is_current_user(user_id):
    return token_service.get_current_user_id() is int(user_id)


def send_confirmation_email(email):
    confirmation_token = token_service.generate_confirmation_token(email)
    confirm_url = url_for('user.confirm_account', token=confirmation_token, _external=True)
    html = render_template('account_confirmation.html', url=confirm_url)
    mail_service.send_email(email, 'Confirm your email', html)
