from flask import Blueprint, request, jsonify

from development.database import db_session
from exceptions.exceptions import MissingRequiredArgumentException, InvalidDataException, ResourceNotFoundException, \
    InsufficientPermissionsException, InvalidEnumConstant, ResourceAlreadyExistsException
from helpers import permission_helpers, location_helpers, validation_helpers
from model.db.models import Team, MatchInvitation, Match
from model.decorators import authentication_required, account_confirmation_required
from model.enums import InvitationStatus
from services import notification_service

match_invitation_view = Blueprint(name='match_invitation', import_name=__name__)


@match_invitation_view.route('/match-invitations', methods=['POST'])
@authentication_required
@account_confirmation_required
def create_invitation():
    req_data = request.get_json()

    try:
        host_id = req_data['inviting_team_id']
        away_id = req_data['invited_team_id']
        coordinates = req_data['location']
        date = req_data['date']
    except KeyError as exc:
        raise MissingRequiredArgumentException(msg='Missing required argument {}'.format(str(exc)))

    validation_helpers.validate_datetime_is_iso_format(date)
    validation_helpers.validate_datetime_is_not_past(date)

    if host_id == away_id:
        raise InvalidDataException(msg='Inviting_team_id and invited_team_id cannot be equal', status_code=422)

    away, host = get_teams(away_id, host_id)
    verify_user_is_team_owner(host)
    location_name = get_and_verify_location(coordinates)

    invitation = MatchInvitation(location=location_name, lat=coordinates['lat'], lng=coordinates['lng'], date=date,
                                 host=host, away=away)
    db_session.add(invitation)
    db_session.flush()

    notifications = notification_service.get_match_invitation_notifications(host, away, invitation)
    db_session.add_all(notifications)
    db_session.commit()
    return jsonify(invitation.serializable)


@match_invitation_view.route('/match-invitations/<id>/status', methods=['PUT'])
@authentication_required
@account_confirmation_required
def change_invitation_status(id):
    try:
        status = request.get_json()['status']
    except KeyError as exc:
        raise MissingRequiredArgumentException(msg='Missing required argument {}'.format(str(exc)))
    try:
        status = InvitationStatus[status.upper()]
    except KeyError as exc:
        raise InvalidEnumConstant(str(exc), InvitationStatus)

    invitation = MatchInvitation.query.filter_by(id=id).first()
    if not invitation:
        raise ResourceNotFoundException(cls=MatchInvitation, value=id)

    verify_user_is_team_owner(invitation.away)

    if invitation.status is not InvitationStatus.WAITING:
        raise ResourceAlreadyExistsException('Invitation status is: {} and cannot be changed anymore'
                                             .format(invitation.status.name))

    match = None
    if status is InvitationStatus.APPROVED:
        match = Match.from_invitation(invitation)
        db_session.add(match)

    invitation.status = status

    notifications = notification_service.get_match_invitation_status_change(invitation)
    db_session.add_all(notifications)
    db_session.commit()
    return jsonify(dict(invitation=invitation.serializable, match=match.serializable if match else None))


def get_teams(away_id, host_id):
    host = Team.query.filter_by(id=host_id).first()
    if not host:
        raise ResourceNotFoundException(cls=Team, value=host_id)
    away = Team.query.filter_by(id=away_id).first()
    if not away:
        raise ResourceNotFoundException(cls=Team, value=away_id)
    return away, host


def get_and_verify_location(coordinates):
    try:
        location_name = location_helpers.get_location_from_coordinates(coordinates['lat'], coordinates['lng'])
    except KeyError as exc:
        raise InvalidDataException(str(exc))
    return location_name


def verify_user_is_team_owner(team):
    if not permission_helpers.is_current_user_team_leader(team):
        raise InsufficientPermissionsException()

