import datetime

from flask import Blueprint, request, jsonify, Response

from development.database import db_session
from exceptions import exceptions
from exceptions.exceptions import ResourceAlreadyExistsException, InsufficientPermissionsException, \
    ResourceNotFoundException
from helpers import permission_helpers
from model.db.models import Team, User, TeamMembership
from model.decorators import authentication_required, account_confirmation_required
from services import notification_service
from services import token_service

team_membership_view = Blueprint('team_membership', __name__)


def is_user_already_in_team(team_id, user_id):
    memberships = TeamMembership.query.filter_by(team_id=team_id, user_id=user_id).all()
    return len(memberships) > 0


@team_membership_view.route('/team-memberships', methods=['POST'])
@authentication_required
@account_confirmation_required
def join_team():
    try:
        team_id = request.get_json()['team_id']
    except KeyError as exc:
        raise exceptions.MissingRequiredArgumentException("Missing required argument {}".format(str(exc)))

    team = Team.query.filter_by(id=team_id).first()
    if not team:
        raise ResourceNotFoundException(cls=Team, value=team_id)

    user = User.query.filter_by(id=token_service.get_current_user_id()).first()

    if is_user_already_in_team(team_id, user.id):
        raise ResourceAlreadyExistsException(msg='User is already in this team or there is already a request for '
                                                 'joining this team')
    membership = TeamMembership(team=team, user=user)
    db_session.add(membership)
    db_session.flush()

    notifications = notification_service.get_team_join_request_notifications(user, team, membership)
    db_session.add_all(notifications)
    db_session.commit()
    return jsonify(membership.serializable)


@team_membership_view.route('/team-memberships/<membership_request_id>', methods=['DELETE'])
@authentication_required
@account_confirmation_required
def leave_team(membership_request_id):
    membership_request = TeamMembership.query.filter_by(id=membership_request_id).first()
    if not membership_request:
        raise ResourceNotFoundException(cls=TeamMembership, value=membership_request_id)

    if not permission_helpers.is_current_user_team_leader(membership_request.team):
        if membership_request.user_id is not token_service.get_current_user_id():
            raise InsufficientPermissionsException()

    notification = notification_service.get_team_join_request_rejection_notification(membership_request)

    db_session.add(notification)
    db_session.delete(membership_request)
    db_session.commit()
    return Response(status=204)


@team_membership_view.route('/team-memberships/<membership_request_id>/confirm', methods=['POST'])
@authentication_required
@account_confirmation_required
def change_status(membership_request_id):
    membership_request = TeamMembership.query.filter_by(id=membership_request_id).first()
    if not membership_request:
        raise ResourceNotFoundException(cls=TeamMembership, value=membership_request_id)

    team = membership_request.team
    if not permission_helpers.is_current_user_team_leader(team):
        raise InsufficientPermissionsException()

    if membership_request.confirmed:
        raise ResourceAlreadyExistsException(msg="Team membership is already confirmed")

    membership_request.start_date = datetime.date.today()
    membership_request.confirmed = True

    notification = notification_service.get_team_join_request_approval_notification(membership_request)
    db_session.add(notification)
    db_session.commit()
    return jsonify(membership_request.serializable)



