import datetime
from math import ceil

from flask import jsonify, request, Blueprint, Response, current_app

from development.database import db_session
from exceptions.exceptions import MissingRequiredArgumentException, InvalidEnumConstant, InvalidDataException, \
    InsufficientPermissionsException, ResourceNotFoundException
from helpers import files_helpers, location_helpers, db_helpers, permission_helpers, request_helpers
from model import enums
from model.db.models import Team, TeamMembership, User
from model.decorators import authentication_required, account_confirmation_required
from model.enums import Disciplines, InvitationStatus
from services import token_service

team_view = Blueprint('team', __name__)


@team_view.route('/teams', methods=['GET'])
@authentication_required
@account_confirmation_required
def get_all_teams():
    # pagination params
    page = request.args.get('page', default=1, type=int)
    page_size = request.args.get('per_page', default=current_app.config['ITEMS_PER_PAGE'], type=int)
    # filtering params
    name = request.args.get('name', default='%%', type=str)
    coordinates = (request.args.get('lat', default=0, type=float), request.args.get('lng', default=0, type=float))
    radius = request.args.get('radius', default=0, type=float)
    # sorting param
    sort_by = request.args.get('order_by', default='', type=str)

    try:
        teams_query = Team.query.filter(Team.name.ilike(name))
        if coordinates[0] and coordinates[1] and radius:
            teams_query = teams_query.filter(Team.check_if_in_radius(latlng=coordinates, radius=radius))
            if not sort_by:
                teams_query = teams_query.order_by(Team.get_great_circe_distance(latlng=coordinates))
            else:
                teams_query = teams_query.order_by(getattr(Team, sort_by))
        else:
            teams_query = teams_query.order_by(getattr(Team, sort_by if sort_by else 'name'))

        result = teams_query.paginate(page, page_size, True)
    except AttributeError as exc:
        raise InvalidDataException(str(exc))

    page_count = ceil(result.total / page_size)

    return jsonify(data=get_serialized_team_list(result.items),
                   pagination=dict(current_page=page, page_count=page_count, total_item_count=result.total))


@team_view.route('/teams/<team_id>', methods=['GET'])
@authentication_required
@account_confirmation_required
def get_team(team_id):
    team = Team.query.filter(Team.id == team_id).first()
    if not team:
        raise ResourceNotFoundException(cls=Team, value=team_id)
    return jsonify(get_serialized_team(team))


@team_view.route('/teams', methods=['POST'])
@authentication_required
@account_confirmation_required
def add_team():
    req_data = request.get_json()
    # required parameters
    try:
        name = req_data['name']
        discipline = req_data['discipline']
    except KeyError as exc:
        raise MissingRequiredArgumentException(msg='Missing required argument {}'.format(str(exc)))

    # optional parameters
    description = req_data['description'] if 'description' in req_data else None
    coordinates = req_data['location'] if 'location' in req_data else None

    try:
        # TODO catch exception thrown when discipline is not a string
        discipline = Disciplines[discipline.upper()]
    except KeyError as exc:
        raise InvalidEnumConstant(str(exc), Disciplines)

    team = Team(name=name, description=description, discipline=discipline)
    user = User.query.filter_by(id=token_service.get_current_user_id()).first()

    team_membership = TeamMembership(team=team, user=user, role=enums.Roles.LEADER, confirmed=True,
                                     start_date=datetime.date.today())
    location_helpers.set_team_location(coordinates, team)

    db_session.add(team)
    db_session.add(team_membership)
    db_helpers.commit_unique_safe()
    return jsonify(get_serialized_team(team))


@team_view.route('/teams/<team_id>', methods=['PUT'])
@authentication_required
@account_confirmation_required
def update_team(team_id):
    team = Team.query.filter(Team.id == team_id).first()
    if not team:
        raise ResourceNotFoundException(cls=Team, value=team_id)

    if not permission_helpers.is_current_user_team_leader(team):
        raise InsufficientPermissionsException()

    req_data = request.get_json()
    try:
        name = req_data['name']
        coordinates = req_data['location']
        description = req_data['description']
        discipline = req_data['discipline']
    except KeyError as exc:
        raise MissingRequiredArgumentException(msg='Missing required argument {}'.format(str(exc)))

    try:
        # TODO catch exception thrown when discipline is not a string
        discipline = Disciplines[discipline.upper()]
    except KeyError as exc:
        raise InvalidEnumConstant(str(exc), Disciplines)

    team.name = name
    team.description = description
    team.discipline = discipline
    location_helpers.set_team_location(coordinates, team)

    db_helpers.commit_unique_safe()
    return jsonify(get_serialized_team(team))


@team_view.route('/teams/<team_id>', methods=['DELETE'])
@authentication_required
@account_confirmation_required
def delete_team(team_id):
    team = Team.query.filter(Team.id == team_id).first()
    if not team:
        raise ResourceNotFoundException(cls=Team, value=team_id)
    if not permission_helpers.is_current_user_team_leader(team):
        raise InsufficientPermissionsException()
    db_session.delete(team)
    db_session.commit()
    return Response(status=204)


@team_view.route('/teams/<team_id>/team-memberships')
@authentication_required
@account_confirmation_required
def get_teams_memberships(team_id):
    team_memberships = TeamMembership.query.filter_by(team_id=team_id).all()
    return jsonify([dict(t.serializable) for t in team_memberships])


@team_view.route('/teams/<team_id>/thumbnail', methods=['POST'])
@authentication_required
@account_confirmation_required
def upload_thumbnail(team_id):
    team = Team.query.filter(Team.id == team_id).first()
    if not team:
        raise ResourceNotFoundException(cls=Team, value=team_id)
    if not permission_helpers.is_current_user_team_leader(team):
        raise InsufficientPermissionsException()
    file_path = files_helpers.parse_and_save_thumbnail(team_id, Team)
    return jsonify(thumbnail=file_path)


@team_view.route('/teams/<team_id>/thumbnail', methods=['DELETE'])
@authentication_required
@account_confirmation_required
def delete_thumbnail(team_id):
    team = Team.query.filter(Team.id == team_id).first()
    if not team:
        raise ResourceNotFoundException(cls=Team, value=team_id)
    if not permission_helpers.is_current_user_team_leader(team):
        raise InsufficientPermissionsException()
    files_helpers.parse_and_delete_thumbnail(team_id, Team)
    return Response(status=204)


@team_view.route('/teams/<team_id>/match-invitations')
@authentication_required
@account_confirmation_required
def get_all_match_invitations(team_id):
    team = Team.query.filter(Team.id == team_id).first()
    if not team:
        raise ResourceNotFoundException(cls=Team, value=team_id)
    if not permission_helpers.is_current_user_team_leader(team):
        raise InsufficientPermissionsException()

    invitations = [i for i in team.invitations if i.status is InvitationStatus.WAITING]
    invitations.sort(key=lambda i: i.create_date, reverse=True)
    return jsonify([dict(invitation.serializable) for invitation in invitations])


@team_view.route('/teams/<team_id>/matches')
@authentication_required
@account_confirmation_required
def get_all_matches(team_id):
    team = Team.query.filter(Team.id == team_id).first()
    time = request.args.get('time', default='', type=str)

    if not team:
        raise ResourceNotFoundException(cls=Team, value=team_id)
    permission_helpers.verify_user_belongs_to_one_of_the_teams([team])

    filtered_matches = request_helpers.filter_matches_by_time(team.matches, time)

    return jsonify([dict(m.serializable) for m in filtered_matches])


def get_serialized_team_list(teams):
    return [dict(c.serializable) for c in teams]


def get_serialized_team(team):
    return dict(team.serializable)
