import datetime
from unittest import TestCase

from exceptions.exceptions import InvalidDataException
from helpers import validation_helpers


class TestValidationHelpers(TestCase):

    def test_validate_datetime_is_iso_format(self):
        validation_helpers.validate_datetime_is_iso_format('2004-02-12T15:19:21+00:00')
        validation_helpers.validate_datetime_is_iso_format('2004-02-12T15:19+00:00')
        validation_helpers.validate_datetime_is_iso_format('2004-02-12T15:19')

    def test_validate_datetime_is_iso_format_negative(self):
        with self.assertRaises(InvalidDataException):
            validation_helpers.validate_datetime_is_iso_format('2004-02-12X15:19:21+00:00')
        with self.assertRaises(InvalidDataException):
            validation_helpers.validate_datetime_is_iso_format('2004-13-12T15:19+00:00')
        with self.assertRaises(InvalidDataException):
            validation_helpers.validate_datetime_is_iso_format('2004-01-12')
        with self.assertRaises(InvalidDataException):
            validation_helpers.validate_datetime_is_iso_format('2004-01-12T')
        with self.assertRaises(InvalidDataException):
            validation_helpers.validate_datetime_is_iso_format('2004-01-12T18')
        with self.assertRaises(InvalidDataException):
            validation_helpers.validate_datetime_is_iso_format('2004-13-12T18:18')

    def test_validate_datetime_is_not_past(self):
        validation_helpers.validate_datetime_is_not_past('2019-02-12T15:19:21+01:00')
        validation_helpers.validate_datetime_is_not_past('2019-02-12T15:19:21')
        validation_helpers.validate_datetime_is_not_past('2019-02-12T15:19')

    def test_validate_datetime_is_not_past_negative(self):
        with self.assertRaises(InvalidDataException):
            validation_helpers.validate_datetime_is_not_past('2013-02-12T15:19')
        with self.assertRaises(InvalidDataException):
            validation_helpers.validate_datetime_is_not_past('2013-02-12T15:19+03:00')
        with self.assertRaises(InvalidDataException):
            validation_helpers.validate_datetime_is_not_past('2013-02-12T15:19:21')
        with self.assertRaises(InvalidDataException):
            validation_helpers.validate_datetime_is_not_past(str(datetime.datetime.utcnow() - datetime.timedelta(seconds=1)))


