import datetime
from unittest import TestCase

from helpers import request_helpers
from model.db.models import Match


class TestRequestHelpers(TestCase):
    MATCHES = [
        Match(date=datetime.datetime(2017, 12, 1, 12, 0)),
        Match(date=datetime.datetime(2019, 10, 21, 12, 0)),
        Match(date=datetime.datetime(2021, 1, 31, 12, 0)),
        Match(date=datetime.datetime(2015, 3, 7, 12, 0)),
        Match(date=datetime.datetime(2013, 6, 4, 12, 0)),
        Match(date=datetime.datetime(2011, 2, 4, 12, 0)),
    ]

    def test_filter_matches_by_time(self):
        past_matches = request_helpers.filter_matches_by_time(self.MATCHES, 'past')
        future_matches = request_helpers.filter_matches_by_time(self.MATCHES, 'future')
        all_matches = request_helpers.filter_matches_by_time(self.MATCHES, 'all')

        self.assertEqual(past_matches.__len__(), 4)
        self.assertEqual(future_matches.__len__(), 2)
        self.assertEqual(all_matches.__len__(), 6)