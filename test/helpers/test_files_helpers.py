import os
import tempfile
from unittest import TestCase
from unittest.mock import patch

from exceptions.exceptions import InvalidDataException
from helpers import files_helpers
from webapp import app


class TestFileHelpers(TestCase):

    def setUp(self):
        self.db_fd, app.config['DATABASE'] = tempfile.mkstemp()
        app.testing = True
        self.app = app.test_client()

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(app.config['DATABASE'])

    def test_allowed_file(self):
        with app.app_context():
            app.config['ALLOWED_EXTENSIONS'] = {'png', 'jpg', 'jpeg'}
            files_helpers.check_if_file_is_allowed('dad.jpg')
            files_helpers.check_if_file_is_allowed('lnas.jpeg')
            files_helpers.check_if_file_is_allowed('j231n3mdodak.png')

    def test_allowed_file_negative(self):
        with app.app_context():
            app.config['ALLOWED_EXTENSIONS'] = {'pdf'}
            with self.assertRaises(InvalidDataException):
                files_helpers.check_if_file_is_allowed('udbau.jpg')
            with self.assertRaises(InvalidDataException):
                files_helpers.check_if_file_is_allowed('udbau.pdaf')
            with self.assertRaises(InvalidDataException):
                files_helpers.check_if_file_is_allowed('udbau.')

    @patch('os.listdir')
    def test_is_filename_unique(self, mock_listdir):
        with app.app_context():
            app.config['UPLOAD_FOLDER'] = ''
            mock_listdir.return_value = ['first.jpg', 'second.png']
            self.assertEquals(True, files_helpers.is_filename_unique('1aaa'))
            self.assertEquals(True, files_helpers.is_filename_unique('first.jpg1'))
            self.assertEquals(False, files_helpers.is_filename_unique('first.jpg'))
            self.assertEquals(False, files_helpers.is_filename_unique('second.png'))

    def test_append_random_string(self):
        self.assertEquals(12, len(files_helpers.append_random_string('aa')))
        self.assertEquals(10, len(files_helpers.append_random_string('')))
