from unittest import TestCase

from helpers import location_helpers


class TestLocationHelpers(TestCase):
    WARSAW = (52.240812, 21.014737)
    NEW_YORK = (40.7410861, -73.9896297241625)
    PLONSK = (52.619862, 20.376828)

    def test_get_location_from_coordinates_should_return_valid_towns(self):
        self.assertEqual(location_helpers.get_location_from_coordinates(self.NEW_YORK[0], self.NEW_YORK[1]), 'Nowy Jork')
        self.assertEqual(location_helpers.get_location_from_coordinates(self.WARSAW[0], self.WARSAW[1]), 'Warszawa')
        self.assertEqual(location_helpers.get_location_from_coordinates(self.PLONSK[0], self.PLONSK[1]), 'Płońsk')
        self.assertEqual(location_helpers.get_location_from_coordinates(49.713948, 21.809794), 'Korczyna')
        self.assertEqual(location_helpers.get_location_from_coordinates(53.527892, 23.436111), 'Makowlany')

    def test_get_location_from_coordinates_should_throw_exception_when_cannot_get_exact_location(self):
        with self.assertRaises(ValueError):
            location_helpers.get_location_from_coordinates(25.1935066,17.0685217)

    def test_get_distance_between_coordinates(self):
        self.assertAlmostEqual(location_helpers.get_distance_between_coordinates(self.WARSAW, self.PLONSK), 60, delta=1)
        self.assertAlmostEqual(location_helpers.get_distance_between_coordinates(self.WARSAW, self.NEW_YORK), 6856, delta=60)


