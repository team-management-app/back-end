from time import sleep
from unittest import TestCase

from exceptions.exceptions import InvalidDataException
from services import token_service
from webapp import app


class TestTokenService(TestCase):
    TEST_MAIL = '123@gmail.com'

    def setUp(self):
        with app.app_context():
            app.config['SECURITY_PASSWORD_SALT'] = '1A'
            app.config['SECRET_KEY'] = 'FF'

    def test_confirm_token_positive(self):
        with app.app_context():
            token = token_service.generate_confirmation_token(self.TEST_MAIL)
            mail = token_service.confirm_token(token)
            self.assertEqual(self.TEST_MAIL, mail)

    def test_confirm_token_invalid(self):
        with app.app_context():
            token = token_service.generate_confirmation_token(self.TEST_MAIL + 'x')
            mail = token_service.confirm_token(token)
            self.assertNotEqual(self.TEST_MAIL, mail)

    def test_confirm_token_expired(self):
        with app.app_context():
            with self.assertRaises(InvalidDataException):
                token = token_service.generate_confirmation_token(self.TEST_MAIL)
                sleep(2)
                token_service.confirm_token(token, 1)


