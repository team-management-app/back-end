#!/bin/sh
source venv/bin/activate

sleep 10s
#flask db upgrade
#flask translate compile
exec gunicorn -b :5000 --timeout 120 --access-logfile - --error-logfile - webapp:app