Show single team
----

* **URL**

    /teams/{id}

* **Method:**


  `GET`

*  **URL Params**


   **Required:**

   `id=[integer]`

   **Optional:**


* **Data Params**

* **Success Response:**


  * **Code:** 200 <br />
   **Content:**
  ```json
  {
        "description": null,
        "discipline": "football",
        "id": 12,
        "location": "Płock",
        "name": "Wisła Płock",
        "players": [{
                "born_date": "28-02-2004",
                "first_name": "Adrian",
                "id": 14,
                "last_name": "Muzyk",
                "thumbnail": null
            },
            {
                "born_date": "16-08-1980",
                "first_name": "Filip",
                "id": 19,
                "last_name": "Kulisz",
                "thumbnail": null
            },
            {
                "born_date": "23-06-2012",
                "first_name": "Julita",
                "id": 42,
                "last_name": "Huszcza",
                "thumbnail": null
            },
            {
                "born_date": "11-04-1999",
                "first_name": "Krystyna",
                "id": 54,
                "last_name": "Molek",
                "thumbnail": null
            },
            {
                "born_date": "16-05-1989",
                "first_name": "Stanisław",
                "id": 96,
                "last_name": "Pakosz",
                "thumbnail": null
            }
        ],
        "thumbnail": "static/thumbnails/CZHZXGLUGLyoutube-miniatura-filmow-thumbnail-jak-utworzyc-dlaczego-warto.jpg"
  }
    ```

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** Empty <br />
    **Info:** Team not found

  * **Code:** 401 UNAUTHORIZED<br />
    **Content:** `{
    "error": {
        "message": "'Authentication token is expired'"}}`

  * **Code:** 403 FORBIDDEN<br />
    **Content:** `{
    "error": {
      "message": "Account is not confirmed"
    }
    }`


