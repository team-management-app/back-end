Create team
----

* **URL**

    /teams

* **Method:**


  `POST`

*  **URL Params**


* **Data Params**

    **Required:**

   `name=[string]` <br>Must be unique<br>
   `discipline=[string]` <br>
   One of \[FOOTBALL, BASKETBALL, HANDBALL, VOLLEYBALL]

   **Optional:**

   `description=[string]`<br>
   `location=[object]`

   Location examples:
   ```json
   "location": {
        "name": "Gdansk"
   }
   ```
   ```json
   "location": {
        "lat": 54.33,
        "lng": 18.63
   }
   ```
    Name field has higher priority than lat&lng.


* **Success Response:**


  * **Code:** 200 <br />
   **Content:**
  ```json
    {
        "description": "My new team",
        "discipline": "football",
        "id": 30,
        "location": "Gdańsk",
        "name": "test_team",
        "thumbnail": null
    }
    ```

* **Error Response:**

  * **Code:** 400 BAD REQUEST<br />
    **Content:** `{
    "error": {
        "message": "Missing required argument 'name'"
    }
    }`

   * **Code:** 409 CONFLICT<br />
    **Content:** `{
    "error": {
      "message": "Duplicated unique value! DETAIL:  Key (name)=(test_team) already exists."
    }
}`
    * **Code:** 401 UNAUTHORIZED<br />
    **Content:** `{
    "error": {
        "message": "'Authentication token is expired'"}}`

  * **Code:** 403 FORBIDDEN<br />
    **Content:** `{
    "error": {
      "message": "Account is not confirmed"
    }
    }`



