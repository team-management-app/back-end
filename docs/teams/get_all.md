Show all teams
----

* **URL**

    /teams

* **Method:**


  `GET`

*  **URL Params**


   **Required:**

   **Optional:**

    * `page=[integer]` <br />
    Current page number(default: 1)
    * `per_page=[integer]`<br />
    Items per page(default: 10)
    * `name=[string]` <br />
    List teams which names exactly matches the string. Proceeding or exceeding the string with '%' allows seeking for all names containing the string.
    * `lat=[float]`
    * `lng=[float]`
    * `radius=[float]` <br />
    List teams(sorted by distance by default) which are located within radius of the lat&lng coordinates <br />
    All three above parameters must be present.
    * `order_by=[str]`<br />
    List teams sorted by given string. One of \[name, location(lexicographical order), thumbail(probably useless)]
* **Data Params**

* **Success Response:**


  * **Code:** 200 <br />
   **Content:**
  ```json
  {
    "data": [
        {
            "description": null,
            "discipline": "football",
            "id": 13,
            "location": "Gdynia",
            "name": "Arka Gdynia",
            "players": [
                {
                    "href": "/users/12"
                },
                {
                    "href": "/users/18"
                },
                {
                    "href": "/users/45"
                },
                {
                    "href": "/users/83"
                },
                {
                    "href": "/users/85"
                },
                {
                    "href": "/users/87"
                }
            ],
            "thumbnail": null
        },
        {
            "description": null,
            "discipline": "football",
            "id": 8,
            "location": "Nieciecza",
            "name": "Bruk-Bet Termalica Nieciecza",
            "players": [
                {
                    "href": "/users/1"
                },
                {
                    "href": "/users/16"
                },
                {
                    "href": "/users/35"
                },
                {
                    "href": "/users/48"
                },
                {
                    "href": "/users/51"
                },
                {
                    "href": "/users/67"
                },
                {
                    "href": "/users/72"
                },
                {
                    "href": "/users/86"
                },
                {
                    "href": "/users/93"
                }
            ],
            "thumbnail": null
        }
    ],
    "pagination": {
        "current_page": 1,
        "page_count": 8
    }
  }
    ```

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:**`{
    "error": {
        "message": "type object 'Team' has no attribute 'number_of_players'"
      }
    }`

  * **Code:** 401 UNAUTHORIZED<br />
    **Content:** `{
    "error": {
        "message": "'Authentication token is expired'"}}`

  * **Code:** 403 FORBIDDEN<br />
    **Content:** `{
    "error": {
      "message": "Account is not confirmed"
    }
    }`