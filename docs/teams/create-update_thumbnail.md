Create or update thumbnail
----

* **URL**

    /teams/{id}/thumbnail

* **Method:**


  `POST`

*  **URL Params**

   `id=[integer]`

* **Data Params**

    **Required:**

   `file=[file]` <br>multipart<br>
   `filename=[string]` <br>
   File must have one of the following extensions: png, jpg, jpeg

   **Optional:**

* **Success Response:**


  * **Code:** 200 <br />
   **Content:**
  ```json
    {
        "thumbnail": "static/thumbnails/99WD6MW9UVyoutube-miniatura.jpg"
    }
    ```

* **Error Response:**

  * **Code:** 400 BAD REQUEST<br />
    **Content:** `{
    "error": {
        "message": "Missing required argument 'file'""
    }
    }`

  * **Code:** 400 BAD REQUEST<br />
    **Content:** `{
    "error": {
            "message": "Invalid file extension. Must be one of {'jpeg', 'jpg', 'png'}"
    }
    }`

   * **Code:** 413 Request Entity Too Large<br />
    **Content:** `{
    "error": {
      "message": "The data value transmitted exceeds the capacity limit"
    }
}`
    * **Code:** 401 UNAUTHORIZED<br />
    **Content:** `{
    "error": {
        "message": "'Authentication token is expired'"}}`

  * **Code:** 403 FORBIDDEN<br />
    **Content:** `{
    "error": {
      "message": "Account is not confirmed"
    }
    }`



