Get all user's notifications
----

* **URL**

    /user/<user_id>/notifications

* **Method:**


  `GET`

*  **URL Params**

   **Required:**

   `user_id[int]`

* **Data Params**


* **Success Response:**


  * **Code:** 200 <br />
   **Content:**
  ```json
    [
        {
            "create_time": "2018-10-22T19:24:38.235501",
            "data": null,
            "id": 2,
            "recipient": {
                "born_date": null,
                "first_name": "Jack",
                "id": 101,
                "last_name": "Kowalsky",
                "thumbnail": null
            },
            "sender": {
                "born_date": null,
                "first_name": "Mike",
                "id": 17,
                "last_name": "Thunderstorm",
                "thumbnail": null
            },
            "type": "TEAM_MEMBERSHIP_APPROVAL"
        }
    ]
    ```
    Type is one of: INFO <br>
    TEAM_JOIN_REQUEST <br>
    MATCH_REQUEST <br>
    TEAM_MEMBERSHIP_APPROVAL <br>
    TEAM_MEMBERSHIP_REJECTION <br>

* **Error Response:**

    * **Code:** 401 UNAUTHORIZED<br />
    **Content:** `{
    "error": {
        "message": "'Authentication token is expired'"}}`

  * **Code:** 403 FORBIDDEN<br />
    **Content:** `{
    "error": {
      "message": "Account is not confirmed"
    }
    }`



