Delete notification
----

* **URL**

    /notifications/{notification_id}

* **Method:**


  `DELETE`

*  **URL Params**


   **Required:**

   `notification_id=[integer]`

   **Optional:**


* **Data Params**

* **Success Response:**


  * **Code:** 204 <br />
   **Content:** Empty

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** Empty <br />
    **Info:** Notification not found

  * **Code:** 401 UNAUTHORIZED<br />
    **Content:** `{
    "error": {
        "message": "'Authentication token is expired'"}}`

  * **Code:** 403 FORBIDDEN<br />
    **Content:** `{
    "error": {
      "message": "Account is not confirmed"
    }
    }`

