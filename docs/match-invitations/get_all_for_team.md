Get all match invitations for a team
------------------------------------
Only team leader can perform this operation.
Only invitations with status = waiting are returned.

* **URL**

    /teams/<id>/match-invitations

* **Method:**


  `GET`

*  **URL Params**


* **Data Params**

    **Required:**

   `inviting_team_id=[string]` <br>
   `invited_team_id=[string]` <br>
   `location=[object]` <br>
   `date=[datetime]` ISO 8601 <br>


   Location example:
   ```json
   "location": {
        "lat": 54.33,
        "lng": 18.63
   }
   ```


* **Success Response:**


  * **Code:** 200 <br />
   **Content:**
  ```json
  {
    "create_date": "Thu, 29 Nov 2018 15:10:58 GMT",
    "date": "2018-11-29T23:29:00",
    "id": 1,
    "invited_team": {
        "description": null,
        "discipline": "football",
        "id": 2,
        "location": {
            "lat": 53.12750505,
            "lng": 23.1470508701617,
            "name": "Białystok"
        },
        "name": "Jagiellonia Białystok",
        "thumbnail": null
    },
    "inviting_team": {
        "description": null,
        "discipline": "football",
        "id": 1,
        "location": {
            "lat": 52.4006522,
            "lng": 16.9197520238885,
            "name": "Poznań"
        },
        "name": "Lech Poznań",
        "thumbnail": null
    },
    "location": {
        "lat": 54.37,
        "lng": 18.63,
        "name": "Gdańsk"
    },
    "status": "waiting"
  }
    ```

* **Error Response:**

  * **Code:** 400 BAD REQUEST<br />
    **Content:** `{
    "error": {
        "message": "Missing required argument 'location'"
    }
    }`

   * **Code:** 409 CONFLICT<br />
    **Content:** `{
    "error": {
      "message": "Duplicated unique value! DETAIL:  Key (name)=(test_team) already exists."
    }
}`
    * **Code:** 401 UNAUTHORIZED<br />
    **Content:** `{
    "error": {
        "message": "'Authentication token is expired'"}}`

  * **Code:** 403 FORBIDDEN<br />
    **Content:** `{
    "error": {
      "message": "Account is not confirmed"
    }
    }`

  * **Code:** 422 UNPROCESSABLE ENTITY<br />
    **Content:** `{
    "error": {
      "message": "Inviting_team_id and invited_team_id cannot be equal"
    }
    }`



