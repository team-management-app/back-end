Create match invitation
-----------------------
Only team leader can perform this operation

* **URL**

    /match-invitations

* **Method:**


  `POST`

*  **URL Params**

    `id[int]` = team id


* **Data Params**

    **Required:**

* **Success Response:**


  * **Code:** 200 <br />
   **Content:**
  ```json
  [
  {
    "create_date": "Thu, 29 Nov 2018 15:10:58 GMT",
    "date": "2018-11-29T23:29:00",
    "id": 1,
    "invited_team": {
        "description": null,
        "discipline": "football",
        "id": 2,
        "location": {
            "lat": 53.12750505,
            "lng": 23.1470508701617,
            "name": "Białystok"
        },
        "name": "Jagiellonia Białystok",
        "thumbnail": null
    },
    "inviting_team": {
        "description": null,
        "discipline": "football",
        "id": 1,
        "location": {
            "lat": 52.4006522,
            "lng": 16.9197520238885,
            "name": "Poznań"
        },
        "name": "Lech Poznań",
        "thumbnail": null
    },
    "location": {
        "lat": 54.37,
        "lng": 18.63,
        "name": "Gdańsk"
    },
    "status": "waiting"
  }
  ]
    ```

* **Error Response:**

    * **Code:** 401 UNAUTHORIZED<br />
    **Content:** `{
    "error": {
        "message": "'Authentication token is expired'"}}`

  * **Code:** 403 FORBIDDEN<br />
    **Content:** `{
    "error": {
      "message": "Account is not confirmed"
    }
    }`
  * **Code:** 404 NOT FOUND<br />
    **Content:** `{
    "error": {
      "message": "Team with (id = 99) not found"
    }
    }`




