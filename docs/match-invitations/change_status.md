Change status of match invitation
---------------------------------
Only team leader can perform this operation

* **URL**

    /match-invitations/{id}/status

* **Method:**


  `PUT`

*  **URL Params**

    `id[int]` = match invitation id

* **Data Params**

    **Required:**

    `status[enum]` <br>One of: waiting, approved, rejected

* **Success Response:**


  * **Code:** 200 <br />
   **Content:**
  ```json
    {
    "invitation": {
        "create_date": "2018-11-30T16:50:05.010925",
        "date": "2019-11-30T23:29:00",
        "id": 6,
        "invited_team": {
            "description": null,
            "discipline": "football",
            "id": 2,
            "location": {
                "lat": 53.12750505,
                "lng": 23.1470508701617,
                "name": "Białystok"
            },
            "name": "Jagiellonia Białystok",
            "thumbnail": null
        },
        "inviting_team": {
            "description": null,
            "discipline": "football",
            "id": 1,
            "location": {
                "lat": 52.4006522,
                "lng": 16.9197520238885,
                "name": "Poznań"
            },
            "name": "Lech Poznań",
            "thumbnail": null
        },
        "location": {
            "lat": 54.37,
            "lng": 18.63,
            "name": "Gdańsk"
        },
        "status": "approved"
    },
    "match": {
        "create_date": "2018-11-30T16:51:31.649842",
        "date": "2019-11-30T23:29:00",
        "id": 5,
        "invited_team": {
            "description": null,
            "discipline": "football",
            "id": 2,
            "location": {
                "lat": 53.12750505,
                "lng": 23.1470508701617,
                "name": "Białystok"
            },
            "name": "Jagiellonia Białystok",
            "thumbnail": null
        },
        "inviting_team": {
            "description": null,
            "discipline": "football",
            "id": 1,
            "location": {
                "lat": 52.4006522,
                "lng": 16.9197520238885,
                "name": "Poznań"
            },
            "name": "Lech Poznań",
            "thumbnail": null
        },
        "location": {
            "lat": 54.37,
            "lng": 18.63,
            "name": "Gdańsk"
        }
    }
  }
    ```

* **Error Response:**

  * **Code:** 400 BAD REQUEST<br />
    **Content:** `{
    "error": {
        "message": "Missing required argument 'confirmed'"
    }
    }`

  * **Code:** 401 UNAUTHORIZED<br />
    **Content:** `{
    "error": {
        "message": "'Authentication token is expired'"}}`

  * **Code:** 403 FORBIDDEN<br />
    **Content:** `{
    "error": {
      "message": "Account is not confirmed"
    }
    }`

   * **Code:** 409 CONFLICT<br />
    **Content:** `{
    "error": {
      "message": "Invitation status is APPROVED and cannot be changed anymore"
    }
}`



