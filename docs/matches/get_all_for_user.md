Get all matches for user
------------------------

* **URL**

    /users/{id}/matches

* **Method:**


  `GET`

*  **URL Params**


   **Required:**
    * `id=[integer]` <br />

   **Optional:**
.
    * `time=[str]`<br />
    One of: past, future, all

* **Data Params**

* **Success Response:**


  * **Code:** 200 <br />
   **Content:**
   ```
   [
    {
        "create_date": "2018-11-30T12:06:34.993015",
        "date": "2019-11-30T23:29:00",
        "id": 1,
        "invited_team": {
            "description": null,
            "discipline": "football",
            "id": 2,
            "location": {
                "lat": 53.12750505,
                "lng": 23.1470508701617,
                "name": "Białystok"
            },
            "name": "Jagiellonia Białystok",
            "thumbnail": null
        },
        "inviting_team": {
            "description": null,
            "discipline": "football",
            "id": 1,
            "location": {
                "lat": 52.4006522,
                "lng": 16.9197520238885,
                "name": "Poznań"
            },
            "name": "Lech Poznań",
            "thumbnail": null
        },
        "location": {
            "lat": 54.37,
            "lng": 18.63,
            "name": "Gdańsk"
        }
    },
    {
        "create_date": "2018-11-30T15:31:08.325949",
        "date": "2019-11-30T23:29:00",
        "id": 2,
        "invited_team": {
            "description": null,
            "discipline": "football",
            "id": 2,
            "location": {
                "lat": 53.12750505,
                "lng": 23.1470508701617,
                "name": "Białystok"
            },
            "name": "Jagiellonia Białystok",
            "thumbnail": null
        },
        "inviting_team": {
            "description": null,
            "discipline": "football",
            "id": 1,
            "location": {
                "lat": 52.4006522,
                "lng": 16.9197520238885,
                "name": "Poznań"
            },
            "name": "Lech Poznań",
            "thumbnail": null
        },
        "location": {
            "lat": 54.37,
            "lng": 18.63,
            "name": "Gdańsk"
        }
    },
    {
        "create_date": "2018-11-30T15:32:19.118320",
        "date": "2019-11-30T23:29:00",
        "id": 3,
        "invited_team": {
            "description": null,
            "discipline": "football",
            "id": 2,
            "location": {
                "lat": 53.12750505,
                "lng": 23.1470508701617,
                "name": "Białystok"
            },
            "name": "Jagiellonia Białystok",
            "thumbnail": null
        },
        "inviting_team": {
            "description": null,
            "discipline": "football",
            "id": 1,
            "location": {
                "lat": 52.4006522,
                "lng": 16.9197520238885,
                "name": "Poznań"
            },
            "name": "Lech Poznań",
            "thumbnail": null
        },
        "location": {
            "lat": 54.37,
            "lng": 18.63,
            "name": "Gdańsk"
        }
    },
    {
        "create_date": "2018-11-30T15:40:39.887000",
        "date": "2017-11-30T15:40:07.582000",
        "id": 4,
        "invited_team": {
            "description": null,
            "discipline": "football",
            "id": 1,
            "location": {
                "lat": 52.4006522,
                "lng": 16.9197520238885,
                "name": "Poznań"
            },
            "name": "Lech Poznań",
            "thumbnail": null
        },
        "inviting_team": {
            "description": null,
            "discipline": "football",
            "id": 2,
            "location": {
                "lat": 53.12750505,
                "lng": 23.1470508701617,
                "name": "Białystok"
            },
            "name": "Jagiellonia Białystok",
            "thumbnail": null
        },
        "location": {
            "lat": null,
            "lng": null,
            "name": null
        }
    }
    ]
   ```


* **Error Response:**

  * **Code:** 404 NOT FOUND <br />

  * **Code:** 401 UNAUTHORIZED<br />
    **Content:** `{
    "error": {
        "message": "'Authentication token is expired'"}}`

  * **Code:** 403 FORBIDDEN<br />
    **Content:** `{
    "error": {
      "message": "Account is not confirmed"
    }
    }`