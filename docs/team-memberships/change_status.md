Change status of specific team membership
----
Only team owner can perform this operation

* **URL**

    /team-memberships/<id>/status

* **Method:**


  `POST`

*  **URL Params**

    `id[int]` = team membership id

* **Data Params**

    **Required:**

    `confirmed[bool]`

* **Success Response:**


  * **Code:** 200 <br />
   **Content:**
  ```json
    {
        "active": true,
        "confirmed": true,
        "id": 101,
        "start_date": "2018-10-22",
        "team_id": 16,
        "user_id": 101
    }
    ```

* **Error Response:**

  * **Code:** 400 BAD REQUEST<br />
    **Content:** `{
    "error": {
        "message": "Missing required argument 'confirmed'"
    }
    }`

    * **Code:** 401 UNAUTHORIZED<br />
    **Content:** `{
    "error": {
        "message": "'Authentication token is expired'"}}`

  * **Code:** 403 FORBIDDEN<br />
    **Content:** `{
    "error": {
      "message": "Account is not confirmed"
    }
    }`



