Delete team membership
----

* **URL**

    /team-memberships/{id}

* **Method:**


  `DELETE`

*  **URL Params**


   **Required:**

   `team_membership_id=[integer]`

   **Optional:**


* **Data Params**

* **Success Response:**


  * **Code:** 204 <br />
   **Content:** Empty

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** Empty <br />
    **Info:** Team membership not found

  * **Code:** 401 UNAUTHORIZED<br />
    **Content:** `{
    "error": {
        "message": "'Authentication token is expired'"}}`

  * **Code:** 403 FORBIDDEN<br />
    **Content:** `{
    "error": {
      "message": "Account is not confirmed"
    }
    }`

