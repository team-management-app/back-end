Create team join request
----

* **URL**

    /team-memberships

* **Method:**


  `POST`

*  **URL Params**


* **Data Params**

    **Required:**

   `team_id[int]`

* **Success Response:**


  * **Code:** 200 <br />
   **Content:**
  ```json
    {
    "active": true,
    "confirmed": false,
    "id": 101,
    "start_date": "2018-10-22",
    "team_id": 16,
    "user_id": 101
    }
    ```

* **Error Response:**

  * **Code:** 400 BAD REQUEST<br />
    **Content:** `{
    "error": {
        "message": "Missing required argument 'team_id'"
    }
    }`

   * **Code:** 409 CONFLICT<br />
    **Content:** `{
    "error": {
      "message": "User is already in this team"
    }
}`
    * **Code:** 401 UNAUTHORIZED<br />
    **Content:** `{
    "error": {
        "message": "'Authentication token is expired'"}}`

  * **Code:** 403 FORBIDDEN<br />
    **Content:** `{
    "error": {
      "message": "Account is not confirmed"
    }
    }`



