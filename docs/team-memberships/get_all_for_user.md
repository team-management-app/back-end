Get all user's team memberships
----

* **URL**

    /user/<user_id>/team-memberships

* **Method:**


  `GET`

*  **URL Params**

   **Required:**

   `user_id[int]`

    **Optional:**

    `team_id[int]`= show only team memberships regarding this team

* **Data Params**


* **Success Response:**


  * **Code:** 200 <br />
   **Content:**
  ```json
    [
        {
            "active": true,
            "confirmed": true,
            "id": 1,
            "start_date": "2018-10-22",
            "team_id": 10,
            "user_id": 1
        }
    ]
    ```

* **Error Response:**

    * **Code:** 401 UNAUTHORIZED<br />
    **Content:** `{
    "error": {
        "message": "'Authentication token is expired'"}}`

  * **Code:** 403 FORBIDDEN<br />
    **Content:** `{
    "error": {
      "message": "Account is not confirmed"
    }
    }`



