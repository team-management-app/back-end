Create a user
----

* **URL**

    /users

* **Method:**


  `POST`

*  **URL Params**


* **Data Params**

    **Required:**

   `first_name=[string]` <br>
   `last_name=[string]` <br>
   `email=[string]`
   Must be unique <br>
   `password=[string]`

   **Optional:**

    `born_date=[string]` <br>Format DD-MM-YYYY

* **Success Response:**


  * **Code:** 200 <br>
   **Content:**
    ```
     {
        "born_date": "21-12-2012",
        "first_name": "Jack",
        "id": 104,
        "last_name": "Kowalsky",
        "thumbnail": null
    }
    ```

* **Error Response:**

  * **Code:** 400 BAD REQUEST<br />
    **Content:** `{
    "error": {
        "message": "Missing required argument 'fist_name'"}}`

   * **Code:** 409 CONFLICT<br />
    **Content:** `{
    "error": {
      "message": "User with this email already exists"
    }
    }`
    * **Code:** 400 BAD REQUEST<br />
    **Content:** `{
    "error": {
      "message": "Email is invalid"
    }
    }`



