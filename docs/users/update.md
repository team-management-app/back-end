Update user
----

* **URL**

    /users/{id}

* **Method:**

     `PUT`

*  **URL Params**


* **Data Params**

    **Required:**

   `first_name=[string]`<br>
   `last_name=[string]` <br>
   `born_date=[string]`  Format: DD-MM-YYYY

* **Success Response:**


  * **Code:** 200 <br />
   **Content:**
  ```json
    {
    "born_date": "21-12-2012",
    "email": "tests@gmail.com",
    "first_name": "John",
    "id": 105,
    "last_name": "Kennedy",
    "thumbnail": null
    }
    ```

* **Error Response:**

  * **Code:** 400 BAD REQUEST<br />
    **Content:** `{
    "error": {
        "message": "Missing required argument 'first_name'"
    }
    }`

   * **Code:** 401 UNAUTHORIZED<br />
    **Content:** `{
    "error": {
        "message": "'Authentication token is expired'"}}`

  * **Code:** 403 FORBIDDEN<br />
    **Content:** `{
    "error": {
      "message": "Account is not confirmed"
    }
    }`



