Delete thumbnail
----

* **URL**

    /users/{id}/thumbnail

* **Method:**


  `DELETE`

*  **URL Params**

   `id=[integer]`

* **Data Params**


* **Success Response:**


  * **Code:** 204 <br />
   **Content:**: No content

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** Empty <br />
    **Info:** Team not found

  * **Code:** 400 BAD REQUEST<br />
    **Content:** `{
    "error": {
        "message": "Team does not have any thumbnail"
    }
    }`

  * **Code:** 401 UNAUTHORIZED<br />
    **Content:** `{
    "error": {
        "message": "'Authentication token is expired'"}}`

  * **Code:** 403 FORBIDDEN<br />
    **Content:** `{
    "error": {
      "message": "Account is not confirmed"
    }
    }`




