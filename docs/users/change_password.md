Change user password
----

* **URL**

    /users/{id}/password

* **Method:**

     `PUT`

*  **URL Params**


* **Data Params**

    **Required:**

   `password=[string]`<br>

* **Success Response:**


  * **Code:** 200 <br />
   **Content:**
  ```json
    {
    "born_date": "21-12-2012",
    "email": "tests@gmail.com",
    "first_name": "John",
    "id": 105,
    "last_name": "Kennedy",
    "thumbnail": null
    }
    ```

* **Error Response:**

  * **Code:** 400 BAD REQUEST<br />
    **Content:** `{
    "error": {
        "message": "Missing required argument 'password'"
    }
    }`

   * **Code:** 401 UNAUTHORIZED<br />
    **Content:** `{
    "error": {
        "message": "'Authentication token is expired'"}}`

  * **Code:** 403 FORBIDDEN<br />
    **Content:** `{
    "error": {
      "message": "Account is not confirmed"
    }
    }`



