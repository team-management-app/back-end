Log in
----

* **URL**

    /session

* **Method:**


  `POST`

*  **URL Params**


* **Data Params**

    **Required:**

   `email=[string]` <br>
   `password=[string]`

   **Optional:**


* **Success Response:**


  * **Code:** 200 <br />
   **Content:** `{
    "auth_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MzgyMDI0NDYsImlhdCI6MTUzNjk5Mjg0Niwic3ViIjoxfQ.KgQ6aV53rK-SGDFY-vWfKHOKO-ftQqPOee767QdE_wg"
}
   `


* **Error Response:**

  * **Code:** 404 NOT FOUND<br />
    **Content:** `{
    "error": {
        "message": "User not found"}}`

   * **Code:** 422 UNPROCESSABLE ENTITY<br />
    **Content:** `{
    "error": {
      "message": "Provided password is incorrect"
    }
    }`

   * **Code:** 403 FORBIDDEN<br />
    **Content:** `{
    "error": {
      "message": "Account is not confirmed"
    }
    }`



