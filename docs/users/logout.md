Log out
----

* **URL**

    /session

* **Method:**


  `DELETE`

*  **URL Params**


* **Data Params**

    **Required:**

   **Optional:**


* **Success Response:**


  * **Code:** 204 <br />
   **Content:** No content


* **Error Response:**

  * **Code:** 401 UNAUTHORIZED<br />
    **Content:** `{
    "error": {
        "message": "'Authentication token is expired'"}}`

  * **Code:** 403 FORBIDDEN<br />
    **Content:** `{
    "error": {
      "message": "Account is not confirmed"
    }
    }`

