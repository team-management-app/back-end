Show single user
----

* **URL**

    /users/{id}

* **Method:**


  `GET`

*  **URL Params**


   **Required:**

   `id=[integer]`

   **Optional:**


* **Data Params**

* **Success Response:**


  * **Code:** 200 <br />
   **Content:**
  ```json
  {
    "born_date": "10-06-1974",
    "first_name": "Anita",
    "id": 12,
    "last_name": "Gumuła",
    "teams": [
        {
            "description": null,
            "discipline": "football",
            "id": 2,
            "location": {
                "lat": 53.12750505,
                "lng": 23.1470508701617,
                "name": "Białystok"
            },
            "name": "Jagiellonia Białystok",
            "thumbnail": null
        },
        {
            "description": null,
            "discipline": "football",
            "id": 1,
            "location": {
                "lat": 52.4006522,
                "lng": 16.9197520238885,
                "name": "Poznań"
            },
            "name": "Lech Poznań",
            "thumbnail": null
        }
    ],
    "thumbnail": "static/thumbnails/PZHOU3DMEDdowod.jpg"
    }
    ```

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** Empty <br />
    **Info:** Team not found

  * **Code:** 401 UNAUTHORIZED<br />
    **Content:** `{
    "error": {
        "message": "'Authentication token is expired'"}}`

  * **Code:** 403 FORBIDDEN<br />
    **Content:** `{
    "error": {
      "message": "Account is not confirmed"
    }
    }`


