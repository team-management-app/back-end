from functools import wraps

from exceptions.exceptions import ResourceNotFoundException, \
    AccountNotConfirmedException
from model.db.models import User
from services import token_service


def authentication_required(f):
    @wraps(f)
    def new_function(*args, **kwargs):
        auth_token = token_service.get_token_from_header()
        token_service.decode_auth_token(auth_token)
        return f(*args, **kwargs)
    return new_function


def account_confirmation_required(f):
    @wraps(f)
    def new_function(*args, **kwargs):
        auth_token = token_service.get_token_from_header()
        user_id = token_service.decode_auth_token(auth_token)
        user = User.query.filter_by(id=user_id).first()
        if not user:
            raise ResourceNotFoundException(cls=User,value=user_id)
        if not user.confirmed:
            raise AccountNotConfirmedException()
        return f(*args, **kwargs)
    return new_function
