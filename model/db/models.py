from datetime import datetime

from sqlalchemy import Column, Integer, String, Date, Boolean, DateTime, ForeignKey, Enum, Float, func, PickleType
from sqlalchemy.ext.hybrid import hybrid_method
from sqlalchemy.orm import relationship, backref

from development.database import Base
from model import enums


class Team(Base):
    __tablename__ = 'teams'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(50), unique=True)
    location = Column(String(50))
    lat = Column(Float)
    lng = Column(Float)
    description = Column(String(300))
    create_date = Column(DateTime)
    picture = Column(String)
    discipline = Column(Enum(enums.Disciplines))

    def __init__(self, name=None, location=None, description=None, picture=None, discipline=None, lat=None, lng=None):
        self.name = name
        self.location = location
        self.description = description
        self.create_date = datetime.now()
        self.picture = picture
        self.discipline = discipline
        self.lat = lat
        self.lng = lng

    @property
    def matches(self):
        return self.home_matches + self.away_matches

    @property
    def invitations(self):
        return self.home_match_invitations + self.away_match_invitations

    @hybrid_method
    def check_if_in_radius(self, latlng, radius):
        pass

    @check_if_in_radius.expression
    def check_if_in_radius(cls, latlng, radius):
        return cls.get_great_circe_distance(latlng) <= radius

    @hybrid_method
    def get_great_circe_distance(self, latlng):
        pass

    @get_great_circe_distance.expression
    def get_great_circe_distance(cls, latlng):
        return func.acos(func.sin(func.radians(latlng[0])) * func.sin(func.radians(cls.lat)) +
                         func.cos(func.radians(latlng[0])) * func.cos(func.radians(cls.lat)) *
                         func.cos(func.radians(cls.lng) - (func.radians(latlng[1])))) * 6371

    @property
    def serializable(self):
        return dict(id=self.id, name=self.name, location=dict(name=self.location, lat=self.lat, lng=self.lng),
                    description=self.description, thumbnail=self.picture, discipline=self.discipline.name.lower())


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True, autoincrement=True)
    first_name = Column(String)
    last_name = Column(String)
    born_date = Column(Date)
    email = Column(String)
    password = Column(String)
    salt = Column(String)
    picture = Column(String)
    create_date = Column(DateTime)
    confirmed = Column(Boolean)

    def __init__(self, first_name=None, last_name=None, born_date=None, email=None, password=None, picture=None,
                 confirmed=False, salt=None):
        self.first_name = first_name
        self.last_name = last_name
        self.born_date = born_date
        self.email = email
        self.password = password
        self.salt = salt
        self.picture = picture
        self.create_date = datetime.now()
        self.confirmed = confirmed

    @property
    def serializable(self):
        return dict(id=self.id, first_name=self.first_name, last_name=self.last_name, thumbnail=self.picture,
                    born_date=self.born_date.strftime('%d-%m-%Y') if self.born_date else None)

    @property
    def serializable_with_team_list(self):
        return dict(self.serializable, teams=[t.serializable_team for t in self.contracts if t.confirmed])


class TeamMembership(Base):
    __tablename__ = 'team_memberships'
    id = Column(Integer, primary_key=True, autoincrement=True)
    start_date = Column(Date)
    confirmed = Column(Boolean)
    role = Column(Enum(enums.Roles))
    create_date = Column(DateTime)
    team_id = Column(Integer, ForeignKey('teams.id'))
    user_id = Column(Integer, ForeignKey('users.id'))

    team = relationship('Team', backref=backref('players', cascade='all, delete'))
    user = relationship('User', backref=backref('contracts', cascade='all, delete'))

    def __init__(self, start_date=None, team=None, user=None, confirmed=False, role=enums.Roles.PLAYER):
        self.start_date = start_date
        self.team = team
        self.user = user
        self.role = role
        self.create_date = datetime.now()
        self.confirmed = confirmed

    @property
    def serializable(self):
        return dict(id=self.id, start_date=self.start_date.isoformat() if self.start_date else None,
                    team=self.team.serializable, user=self.user.serializable, confirmed=self.confirmed,
                    role=self.role.name.lower())

    @property
    def serializable_user(self):
        return self.user.serializable

    @property
    def serializable_team(self):
        return self.team.serializable


class BlacklistToken(Base):
    __tablename__ = 'blacklist_tokens'

    id = Column(Integer, primary_key=True, autoincrement=True)
    token = Column(String(500), unique=True, nullable=False)
    blacklisted_on = Column(DateTime, nullable=False)

    def __init__(self, token):
        self.token = token
        self.blacklisted_on = datetime.now()

    def __repr__(self):
        return '<id: token: {}'.format(self.token)

    @staticmethod
    def check_blacklist(auth_token):
        # check whether auth token has been blacklisted
        res = BlacklistToken.query.filter_by(token=str(auth_token)).first()
        return res is not None


class Notification(Base):
    __tablename__ = 'notifications'

    id = Column(Integer, primary_key=True, autoincrement=True)
    type = Column(Enum(enums.NotificationsTypes))
    sender_type = Column(Enum(enums.NotificationSenderTypes))
    create_date = Column(DateTime)
    data = Column(PickleType)
    recipient_id = Column(Integer, ForeignKey('users.id'))

    recipient = relationship(User, foreign_keys=recipient_id, backref=backref('notifications', cascade='all, delete'))

    def __init__(self, type=None, data=None, recipient=None, sender_type=None):
        self.type = type
        self.create_date = datetime.utcnow()
        self.data = data
        self.recipient = recipient
        self.sender_type = sender_type

    @property
    def serializable(self):
        return dict(id=self.id, type=self.type.name.lower(), create_time=self.create_date.isoformat(), data=self.data,
                    sender_type=self.sender_type.name.lower())


class MatchInvitation(Base):
    __tablename__ = "match_invitations"

    id = Column(Integer, primary_key=True, autoincrement=True)
    location = Column(String(50))
    lat = Column(Float)
    lng = Column(Float)
    status = Column(Enum(enums.InvitationStatus))
    date = Column(DateTime)
    create_date = Column(DateTime)
    host_id = Column(Integer, ForeignKey('teams.id'))
    away_id = Column(Integer, ForeignKey('teams.id'))

    host = relationship('Team', foreign_keys=host_id, backref=backref('home_match_invitations', cascade='all, delete'))
    away = relationship('Team', foreign_keys=away_id, backref=backref('away_match_invitations', cascade='all, delete'))

    def __init__(self, location=None, lat=None, lng=None, date=None, status=enums.InvitationStatus.WAITING, host=None, away=None):
        self.location = location
        self.lat = lat
        self.lng = lng
        self.date = date
        self.status = status
        self.host = host
        self.away = away
        self.create_date = datetime.utcnow()

    @property
    def serializable(self):
        return dict(id=self.id, location=dict(name=self.location, lat=self.lat, lng=self.lng),
                    status=self.status.name.lower(), inviting_team=self.host.serializable,
                    invited_team=self.away.serializable, date=self.date.isoformat() if self.date else None,
                    create_date=self.create_date.isoformat())


class Match(Base):
    __tablename__ = "matches"

    id = Column(Integer, primary_key=True, autoincrement=True)
    location = Column(String(50))
    lat = Column(Float)
    lng = Column(Float)
    date = Column(DateTime)
    create_date = Column(DateTime)
    host_id = Column(Integer, ForeignKey('teams.id'))
    away_id = Column(Integer, ForeignKey('teams.id'))

    host = relationship('Team', foreign_keys=host_id, backref=backref('home_matches', cascade='all, delete'))
    away = relationship('Team', foreign_keys=away_id, backref=backref('away_matches', cascade='all, delete'))

    def __init__(self, location=None, lat=None, lng=None, date=None, host=None, away=None):
        self.location = location
        self.lat = lat
        self.lng = lng
        self.date = date
        self.host = host
        self.away = away
        self.create_date = datetime.utcnow()

    @classmethod
    def from_invitation(cls, invitation):
        return Match(invitation.location, invitation.lat, invitation.lng, invitation.date,
                     invitation.host, invitation.away)

    @property
    def serializable(self):
        return dict(id=self.id, location=dict(name=self.location, lat=self.lat, lng=self.lng),
                    inviting_team=self.host.serializable, invited_team=self.away.serializable,
                    date=self.date.isoformat() if self.date else None,
                    create_date=self.create_date.isoformat())
