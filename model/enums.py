from enum import Enum, unique


@unique
class Disciplines(Enum):
    FOOTBALL = 1
    BASKETBALL = 2
    VOLLEYBALL = 3
    HANDBALL = 4


@unique
class NotificationsTypes(Enum):
    TEAM_JOIN_REQUEST = 1
    TEAM_MEMBERSHIP_APPROVAL = 2
    TEAM_MEMBERSHIP_REJECTION = 3
    MATCH_INVITATION = 4
    MATCH_INVITATION_APPROVAL = 5
    MATCH_INVITATION_REJECTION = 6

@unique
class Roles(Enum):
    PLAYER = 1
    LEADER = 2


@unique
class NotificationSenderTypes(Enum):
    TEAM = 1
    PLAYER = 2


@unique
class InvitationStatus(Enum):
    WAITING = 0
    APPROVED = 1
    REJECTED = 2
