from flask import Flask
from flask_mail import Mail

from controllers.health_controller import health_view
from controllers.match_controller import match_view
from controllers.match_invitation_controller import match_invitation_view
from controllers.notifications_controller import notifications_view
from controllers.session_controller import session_view
from controllers.team_controller import team_view
from controllers.team_membership_controller import team_membership_view
from controllers.user_controller import user_view
from development.database import db_session
from exceptions.handlers import exceptions_handler

app = Flask('DemCouch')
app.config.from_object('config.BaseConfig')
mail = Mail(app)

app.register_blueprint(health_view)
app.register_blueprint(team_view)
app.register_blueprint(user_view)
app.register_blueprint(exceptions_handler.blueprint)
app.register_blueprint(notifications_view)
app.register_blueprint(team_membership_view)
app.register_blueprint(session_view)
app.register_blueprint(match_invitation_view)
app.register_blueprint(match_view)
app.config.from_object('config.BaseConfig')
fill_db()

# TODO add profiles


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


@app.after_request
def add_necessary_headers(response):
    response.headers["Access-Control-Allow-Origin"] = "*"
    response.headers["Access-Control-Allow-Headers"] = "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    response.headers["Access-Control-Allow-Methods"] = "*"
    return response


if __name__ == '__main__':
    app.run()




