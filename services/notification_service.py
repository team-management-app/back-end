from model.db.models import TeamMembership, Team, User, MatchInvitation, Notification
from model.enums import InvitationStatus, Roles, NotificationsTypes, NotificationSenderTypes


def get_team_join_request_notifications(sender, team, membership):
    leaders_memberships = TeamMembership.query.filter_by(team_id=team.id, role=Roles.LEADER).all()
    notifications = []
    for leader_membership in leaders_memberships:
        notifications.append(Notification(
            type=NotificationsTypes.TEAM_JOIN_REQUEST,
            data=dict(
                sender=dict(cls=User.__name__, id=sender.id),
                team=dict(cls=Team.__name__, id=team.id),
                team_membership=(dict(cls=TeamMembership.__name__, id=membership.id))
            ),
            sender_type=NotificationSenderTypes.PLAYER,
            recipient=leader_membership.user
        ))
    return notifications


# FIXME extract common part of below methods
def get_team_join_request_approval_notification(membership_request):
    notification = Notification(
        type=NotificationsTypes.TEAM_MEMBERSHIP_APPROVAL,
        data=dict(
            sender=dict(cls=Team.__name__, id=membership_request.team.id),
            team_membership=dict(cls=TeamMembership.__name__, id=membership_request.id)
        ),
        sender_type=NotificationSenderTypes.TEAM,
        recipient=membership_request.user
    )
    return notification


def get_team_join_request_rejection_notification(membership_request):
    notification = Notification(
        type=NotificationsTypes.TEAM_MEMBERSHIP_REJECTION,
        data=dict(
            sender=dict(cls=Team.__name__, id=membership_request.team.id),
            team_membership=dict(cls=TeamMembership.__name__, id=membership_request.id)
        ),
        sender_type=NotificationSenderTypes.TEAM,
        recipient=membership_request.user
    )
    return notification


def get_match_invitation_notifications(host, away, invitation):
    leaders_memberships = TeamMembership.query.filter_by(team_id=away.id, role=Roles.LEADER).all()
    notifications = []
    for leader_membership in leaders_memberships:
        notifications.append(Notification(
            type=NotificationsTypes.MATCH_INVITATION,
            data=dict(
                sender=dict(cls=Team.__name__, id=host.id),
                invitation=dict(cls=MatchInvitation.__name__, id=invitation.id)
            ),
            sender_type=NotificationSenderTypes.TEAM,
            recipient=leader_membership.user)
        )
    return notifications


def get_match_invitation_status_change(invitation):
    memberships = []
    if invitation.status is InvitationStatus.WAITING:
        return []

    if invitation.status is InvitationStatus.APPROVED:
        memberships.extend(TeamMembership.query.filter_by(team_id=invitation.host_id, confirmed=True).all())
        memberships.extend(TeamMembership.query.filter_by(team_id=invitation.away_id, confirmed=True).all())
    else:
        memberships.extend(TeamMembership.query.filter_by(team_id=invitation.host_id, role=Roles.LEADER).all())

    notifications = []
    for membership in memberships:
        notifications.append(Notification(
            type=NotificationsTypes.MATCH_INVITATION_APPROVAL if invitation.status is InvitationStatus.APPROVED
            else NotificationsTypes.MATCH_INVITATION_REJECTION,
            data=dict(
                sender=dict(cls=Team.__name__, id=invitation.away_id),
                invitation=dict(cls=MatchInvitation.__name__, id=invitation.id)
            ),
            sender_type=NotificationSenderTypes.TEAM,
            recipient=membership.user
        ))
    return notifications
