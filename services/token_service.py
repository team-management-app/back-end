import datetime
import re

import jwt
from flask import current_app, request
from itsdangerous import URLSafeTimedSerializer, BadTimeSignature

from exceptions.exceptions import InvalidDataException, MissingRequiredArgumentException
from model.db.models import BlacklistToken


def generate_confirmation_token(email):
    serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
    return serializer.dumps(email, salt=current_app.config['SECURITY_PASSWORD_SALT'])


def confirm_token(token, expiration=3600):
    try:
        serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
        email = serializer.loads(
            token,
            salt=current_app.config['SECURITY_PASSWORD_SALT'],
            max_age=expiration
        )
        return email
    except BadTimeSignature:
        raise InvalidDataException(msg='Invalid or expired confirmation token')


def encode_auth_token(user_id):
    """
    Generates the Auth Token
    :return: string
    """
    try:
        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=14),
            'iat': datetime.datetime.utcnow(),
            'sub': user_id
        }
        return jwt.encode(
            payload,
            current_app.config.get('SECRET_KEY'),
            algorithm='HS256'
        )
    except Exception as e:
        # TODO throw correct exception
        return e


def decode_auth_token(auth_token):
    """
    Decodes the auth token
    :param auth_token:
    :return: integer|string
    """
    try:
        payload = jwt.decode(auth_token, current_app.config.get('SECRET_KEY'))
        if BlacklistToken.check_blacklist(auth_token):
            raise InvalidDataException('Authentication token is blacklisted', status_code=401)
        return payload['sub']
    except jwt.ExpiredSignatureError:
        raise InvalidDataException('Authentication token is expired', status_code=401)
    except jwt.InvalidTokenError:
        raise InvalidDataException('Invalid authentication token.', status_code=401)


def get_current_user_id():
    auth_token = get_token_from_header()
    return jwt.decode(auth_token, current_app.config.get('SECRET_KEY'))['sub']


def get_token_from_header():
    auth_header = request.headers.get('Authorization')
    if not auth_header:
        raise MissingRequiredArgumentException("Missing required header 'Authorization'")
    if not re.match(r"^JWT \S+$", auth_header):
        raise InvalidDataException("Invalid format of Authorization header. Correct format: JWT <token>")
    return auth_header.split(' ')[1]