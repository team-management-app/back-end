class DemcouchException(Exception):
    status_code = 500
    message = ''

    def __init__(self):
        Exception.__init__(self)


class MissingRequiredArgumentException(DemcouchException):
    def __init__(self, msg, status_code=400):
        Exception.__init__(self)
        self.message = msg
        self.status_code = status_code


class InvalidEnumConstant(DemcouchException):
    def __init__(self, value, cls, status_code=400):
        Exception.__init__(self)
        self.message = "{} is not a valid enum constant. Must be on of {}".format(value, str([e.name for e in cls]))
        self.status_code = status_code


class CannotDetermineExactLocationException(DemcouchException):
    def __init__(self, msg=None, status_code=400):
        self.message = msg or "Cannot determine exact location"
        self.status_code = status_code


class InvalidDataException(DemcouchException):
    def __init__(self, msg, status_code=400):
        self.message = msg
        self.status_code = status_code


class ResourceAlreadyExistsException(DemcouchException):
    def __init__(self, msg, status_code=409):
        self.message = msg
        self.status_code = status_code


class ResourceNotFoundException(DemcouchException):
    def __init__(self, cls, value, key='id', status_code=404):
        self.message = "{} with ({} = {}) does not exists!".format(cls.__name__,key, value)
        self.status_code = status_code


class AccountNotConfirmedException(DemcouchException):
    def __init__(self):
        self.message = 'Account is not confirmed'
        self.status_code = 403


class InsufficientPermissionsException(DemcouchException):
    def __init__(self):
        self.message = 'You do not have permissions to perform this operation'
        self.status_code = 403


