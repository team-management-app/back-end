from flask import jsonify, Blueprint

from exceptions.exceptions import DemcouchException

blueprint = Blueprint('error_handlers', __name__)


@blueprint.app_errorhandler(DemcouchException)
def handle_demcouch_exception(error):
    return jsonify(error=dict(message=error.message)), error.status_code


@blueprint.app_errorhandler(400)
def handle_400_error(error):
    return jsonify(error=dict(message="Bad request")), 400

@blueprint.app_errorhandler(404)
def handle_404_error(error):
    return jsonify(error=dict(message="Page not found")), 404


@blueprint.app_errorhandler(500)
def handle_500_error(error):
    return jsonify(error=dict(message=str(error).split('\n')[0])), 500


@blueprint.app_errorhandler(413)
def handle_413_error(error):
    return jsonify(error=dict(message="The data value transmitted exceeds the capacity limit")), 413



